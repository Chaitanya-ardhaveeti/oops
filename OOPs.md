# OOPs Concepts:
-> One of the popular approaches to solve a programming problem is by creating objects. This is known as **Object-Oriented Programming (OOP)**.

-> **Object Oriented programming (OOP)** is a programming paradigm that relies on the concept of classes and objects. It is used to structure a software program into simple, reusable pieces of code blueprints called as classes, which are used to create individual instances of objects.

-> The main aim of **OOP** is to bind together the data and the functions that operate on them so that no other part of the code can access this data except that function.

->An object has two characteristics:
* **Attributes**
* **Behaviour**

-> There are many object-oriented programming languages including **JavaScript**, **C++**, **Java**, and **Python**.

## Object-Oriented Programming (OOP) in Python:

->The concept of **OOP** in **Python** focuses on creating reusable code. This concept is also known as *DRY (Don't Repeat Yourself)*.

### Concepts of OOP in Python:


####  Class:

- In a nutshell, classes are essentially user defined data types. Classes are where we create a blueprint for the structure of methods and attributes. Individual objects are instantiated, or created from this blueprint.
- We can think of class as a sketch of a parrot with labels. It contains all the details about the name, colors, size etc. Based on these descriptions, we can study about the parrot. Here, a parrot is an object.

**Syntax**:

    class Parrot:
        pass

#### Object:

- An **Object**  is an instantiation of a class. When class is defined, only the description for the object is defined.

**Syntax**:

    obj = Parrot()


#### Code:

**Python Code for Creating Class and Object**:

    class Parrot:

        species = "bird"

        def __init__(self, name, age):
        self.name = name
        self.age = age

    blu = Parrot("Blu", 10)
    woo = Parrot("Woo", 15)


#### Methods:

- Methods are functions defined inside the body of a class. They are used to define the behaviors of an object.

- When individual objects are instantiated, these objects can call the methods defined in the class.

- The method’s code is defined in the class definition.

#### Code:

**Python Code for implementing methods**:

    class Parrot:
    
        def __init__(self, name, age):
        self.name = name
        self.age = age
    
        def sing(self, song):
        return "{} sings {}".format(self.name, song)

        def dance(self):
            return "{} is now dancing".format(self.name)

    blu = Parrot("Blu", 10)

- In the above code **sing()** and **dance()** are Methods

#### Inheritance:

- Inheritance is a way of creating a new class for using details of an existing class without modifying it. 

- parent classes extend attributes and behaviors to child classes.

- Inheritance supports reusability.

#### Code:

**Python Code to implement *Inheritance***:


    class Bird:
    
        def __init__(self):
            print("Bird is ready")

        def whoisThis(self):
            print("Bird")

        def swim(self):
            print("Swim faster")


    class Penguin(Bird):

        def __init__(self):
            super().__init__()
            print("Penguin is ready")

        def whoisThis(self):
            print("Penguin")

        def run(self):
            print("Run faster")

- In the above code Class **Bird** is a parent class and Class **Penguin** is a child class.

#### Encapsulation:

- Encapsulation means containing all important information inside an object, and only exposing selected information to the outside world.

- This prevents data from direct modification which is called encapsulation. 

- we denote private attributes using underscore as the prefix i.e single _ or double __.

#### Code:

**Python Code to implement Encapsulation:

    class Computer:

    def __init__(self):
        self.__maxprice = 900

    def sell(self):
        print("Selling Price: {}".format(self.__maxprice))

    def setMaxPrice(self, price):
        self.__maxprice = price

    c = Computer()
    c.sell()

    c.__maxprice = 1000
    c.sell()

    c.setMaxPrice(1000)
    c.sell()

- In the above code we tried to modify the price. However, we can't change it because Python treats the **__maxprice** as private attributes.

- To change the value, we have to use a setter function i.e setMaxPrice() which takes price as a parameter.

#### Polymorphism:

- Polymorphism means designing objects to share behaviors. Using inheritance, objects can override shared parent behaviors, with specific child behaviors. 

- Polymorphism allows the same method to execute different behaviors in two ways: method overriding and method overloading.

- Polymorphism is an ability in **OOP** to use a common interface for multiple forms.

#### Code:

**Python Code to implement Polymorphism**:

    class Parrot:

        def fly(self):
            print("Parrot can fly")
    
         swim(self):
            print("Parrot can't swim")

    class Penguin:

        def fly(self):
            print("Penguin can't fly")
    
        def swim(self):
            print("Penguin can swim")


    def flying_test(bird):
        bird.fly()

    blu = Parrot()
    peggy = Penguin()

    flying_test(blu)
    flying_test(peggy)

- In the above program, we defined two classes **Parrot** and **Penguin**. Each of them have a common *fly()* method. However, their functions are different.

- To use polymorphism, we created a common interface **flying_test()** function that takes any object and calls the object's **fly()** method. 

#### Abstraction:

- Abstraction means that the user interacts with only selected attributes and methods of an object. 

- Abstraction is used to hide the internal functionality of the function from the users. 

**Syntax**:

    from abc import ABC  
    class ClassName(ABC):  

















